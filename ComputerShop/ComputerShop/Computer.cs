﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ComputerShop
{
    interface IASSembler
    {
        Computer AssembleComputer();
    }

    class HighEndComputer : IASSembler
    {
        public Computer AssembleComputer()
        {
            Computer pc = new Computer();
            pc.Cpu = new R1800X();
            pc.Drive = new NvMeSSD();
            pc.Monitor = new _4K();
            return pc;
        }
    }
    class MidRangeComputer : IASSembler
    {
        public Computer AssembleComputer()
        {
            Computer pc = new Computer();
            pc.Cpu = new R1500();
            pc.Drive = new SSD();
            pc.Monitor = new _2K();
            return pc;
        }
    }
    class LowEndComputer : IASSembler
    {
        public Computer AssembleComputer()
        {
            Computer pc = new Computer();
            pc.Cpu = new i6100();
            pc.Drive = new HDD();
            pc.Monitor = new FHD();
            return pc;
        }
    }

}
