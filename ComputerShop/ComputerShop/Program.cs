﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerShop
{
    class Program
    {
        static void Main(string[] args)
        {
            out1:
            Console.WriteLine("High Budget or low Budget m8?");
            string s = Console.ReadLine();
            AssemblerFactory ass = new AssemblerFactory();
            Computer pc = ass.FactoryMethod(s).AssembleComputer();
            Console.WriteLine(pc.ToString());
            Console.ReadLine();
            goto out1;
        }
    }

    class Computer
    {
        public ICPU Cpu { get; set; }
        public IMonitor Monitor { get; set; }
        public IDrive Drive { get; set; }

        public override string ToString()
        {
            return "Price: " + (Cpu.Price + Monitor.Price + Drive.Price);
        }
    }

    class AssemblerFactory
    {
        public IASSembler FactoryMethod(string budget)
        {
            switch (budget)
            {
                case "low": return new LowEndComputer();
                case "high": return new HighEndComputer();
                case "mid": return new MidRangeComputer();
                default: return new LowEndComputer();
            }
        }
    }



}
