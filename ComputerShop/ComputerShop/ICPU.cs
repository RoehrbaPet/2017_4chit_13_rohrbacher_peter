﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerShop
{
    interface ICPU
    {
        int Price { get; }
    }
    class R1800X:ICPU
    {
        private int price = 500;
        public int Price {
            get { return price; }
        }
    }

    class R1500:ICPU
    {
        private int price = 200;

        public int Price
        {
            get { return price; }
        }
    }

    class i6100 : ICPU
    {
        private int price = 150;

        public int Price
        {
            get { return price; }
        }
    }
}
