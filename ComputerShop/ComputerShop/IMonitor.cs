﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerShop
{
    interface IMonitor
    {
        int Price { get; }
    }

    class FHD : IMonitor
    {
        private int price = 110;
        public int Price {
            get { return price; }
        }
    }

    class _2K:IMonitor
    {
        private int price = 250;
        public int Price {
            get { return price; } 
        }
    }

    class _4K : IMonitor
    {
        private int price = 400;
        public int Price { get { return price; }  }
    }
}
