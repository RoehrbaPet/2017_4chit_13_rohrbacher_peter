﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ComputerShop
{
    interface IDrive
    {
        int Price { get; }
    }

    class HDD : IDrive
    {
        private int price = 50;

        public int Price
        {
            get { return price; }
        }
    }

    class SSD : IDrive
    {
        private int price = 100;

        public int Price
        {
            get { return price; }
        }
    }

    class NvMeSSD : IDrive
    {
        private int price = 150;

        public int Price
        {
            get { return price; }
        }
    }
}
