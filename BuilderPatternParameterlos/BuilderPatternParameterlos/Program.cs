﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPatternParameterlos
{
    class Program
    {
        static void Main(string[] args)
        {
            PC pc = Director.Construct(new MediumBuilder());
            Console.WriteLine(pc.ToString());
            
        }
    }

    static class Director
    {
        public static PC Construct(Builder builder)
        {
            PC pc = new PC();
            pc.Cpu = builder.GetCPU();
            pc.Drive = builder.GetDrive();
            pc.Gpu = builder.GetGPU();
            pc.Ram = builder.GetRam();
            return pc;
        }
    }

    abstract class Builder
    {
        public abstract ICPU GetCPU();
        public abstract IGPU GetGPU();
        public abstract IRAM GetRam();
        public abstract IDrive GetDrive();
        
    }

    class CheapBuilder:Builder
    {
        public override ICPU GetCPU()
        {
            return new Ryzen3();
        }

        public override IGPU GetGPU()
        {
            return new Rx480();
        }

        public override IRAM GetRam()
        {
            return new _4GB();
        }

        public override IDrive GetDrive()
        {
            return new HDD();
        }
    }
    class MediumBuilder : Builder
    {
        public override ICPU GetCPU()
        {
            return new Ryzen5();
        }

        public override IGPU GetGPU()
        {
            return new Vega64();
        }

        public override IRAM GetRam()
        {
            return new _8GB();
        }

        public override IDrive GetDrive()
        {
            return new SSD();
        }
    }
    class HighEndBuilder : Builder
    {
        public override ICPU GetCPU()
        {
            return new Ryzen7();
        }

        public override IGPU GetGPU()
        {
            return new _1080Ti();
        }

        public override IRAM GetRam()
        {
            return new _16GB();
        }

        public override IDrive GetDrive()
        {
            return new M2SSD();
        }
    }

    struct PC
    {
        public ICPU Cpu { get; set; }
        public IGPU Gpu { get; set; }
        public IRAM Ram { get; set; }
        public IDrive Drive { get; set; }

        public override string ToString()
        {
            return Cpu.ToString() + " " + Gpu.ToString() + " " + Ram.ToString() + " " + Drive.ToString();
        }
    }

    interface ICPU
    {

    }

    class Ryzen7 : ICPU
    {
        public override string ToString()
        {
            return "Ryzen7";
        }
    }

    class Ryzen5 : ICPU
    {
    }

    class Ryzen3 : ICPU { }

    interface IRAM
    {

    }

    class _8GB : IRAM
    {

    }

    class _16GB : IRAM
    {
        public override string ToString()
        {
            return "16Gb";
        }
    }

    class _4GB : IRAM
    {
    }

    interface IGPU
    {

    }

    class _1080Ti : IGPU
    {
        public override string ToString()
        {
            return "1080Ti";
        }
    }
    class Rx480 : IGPU
    { }

    class Vega64 : IGPU
    {
        public override string ToString()
        {
            return "Vega 64";
        }
    }

    class _IGPU : IGPU
    {
    }

    interface IDrive
    {

    }

    class SSD : IDrive
    {
    }

    class HDD : IDrive
    {
    }

    class M2SSD : IDrive
    {
        public override string ToString()
        {
            return "M.2 SSD";
        }
    }
}
