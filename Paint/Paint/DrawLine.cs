﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Paint
{
    class DrawLine:IDrawBehavior
    {
        public Point point1 = null;
        public Shape Draw(Point p, Canvas DrawField, double thickness, Color c)
        { 
            if (point1 != null)
            {
                Line l1 = new Line();
                l1.X1 = point1.X;
                l1.Y1 = point1.Y;
                l1.X2 = p.X;
                l1.Y2 = p.Y;
                var converter = new System.Windows.Media.BrushConverter();
                var brush = (Brush)converter.ConvertFromString(c.ToString());
                l1.Stroke = brush;
                l1.StrokeThickness = thickness;
                point1 = null;
                return l1;
            }
            else
            {
                point1 = p;
                return null;
            }
        }
    }
}
