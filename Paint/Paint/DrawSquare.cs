﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Paint
{
    class DrawSquare:IDrawBehavior
    {
        public Point point1 = null;
        
        
        public Shape Draw(Point p, Canvas DrawField, double thickness, Color c)
        {
            if (point1 != null)
            {
                Rectangle r = new Rectangle();
                var converter = new System.Windows.Media.BrushConverter();
                var brush = (Brush)converter.ConvertFromString(c.ToString());
                r.Stroke = brush;
                r.StrokeThickness = thickness;
                Canvas.SetTop(r, p.Y > point1.Y ? point1.Y : p.Y);
                Canvas.SetLeft(r, p.X > point1.X ? point1.X : p.X);
                Canvas.SetBottom(r, p.Y > point1.Y ? p.Y : point1.Y);
                Canvas.SetRight(r, p.X > point1.X ? p.X : point1.X);
                r.Width = p.X > point1.X ? p.X - point1.X : point1.X - p.X;
                r.Height = p.Y > point1.Y ? p.Y - point1.Y : point1.Y - p.Y;
                DrawField.Children.Add(r);
                
                point1 = null;
                return new Rectangle();
            }
            else
            {
                point1 = p;
                return null;
            }
        }
    }

    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }


    }
}
