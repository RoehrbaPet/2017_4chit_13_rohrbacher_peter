﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Paint
{
    class DrawPoint:IDrawBehavior
    {
        public Point point1;
        public Shape Draw(Point p, Canvas DrawField, double thickness, Color c)
        {
            if (point1 == null)
            {
                point1 = new Point(p.X+1, p.Y+1);
            }
            Line l = new Line();
            l.X1 = p.X;
            l.X2 = point1.X;
            l.Y1 = p.Y;
            l.Y2 = point1.Y;
            l.StrokeThickness = thickness;
            point1 = p;
            var converter = new System.Windows.Media.BrushConverter();
            var brush = (Brush)converter.ConvertFromString(c.ToString());
            l.Stroke = brush;
            return l;
        }
    }
}
