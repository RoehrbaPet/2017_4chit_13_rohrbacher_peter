﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace Paint
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IDrawBehavior drawer = null;
        

        public MainWindow()
        {
            InitializeComponent();
            ClrPcker_Background.SelectedColor = Color.FromRgb(0,0,0);
        }


        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
            Shape s = drawer.Draw(new Point(Mouse.GetPosition(cavDrawField).X, Mouse.GetPosition(cavDrawField).Y), cavDrawField, Convert.ToDouble(Thickness.Text), ClrPcker_Background.SelectedColor.Value);
            if(s != null)
            cavDrawField.Children.Add(s);
            }
            catch (NullReferenceException)
            {

                System.Windows.MessageBox.Show("NullReferenceException encountered. Try Selecting a DrawBehaviour");
            }
            
        }

        private void cavDrawField_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed && drawer is DrawPoint)
            {
                Shape s = drawer.Draw(new Point(Mouse.GetPosition(cavDrawField).X, Mouse.GetPosition(cavDrawField).Y),
                    cavDrawField, Convert.ToDouble(Thickness.Text), ClrPcker_Background.SelectedColor.Value);

                if (s != null)
                    cavDrawField.Children.Add(s);
            }
        }


        private void rdbSquare_MouseDown(object sender, MouseButtonEventArgs e)
        {
           
        }



        private void cavDrawField_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //drawer.Draw(Mouse.GetPosition(cavDrawField),cavDrawField);
        }

        private void cavDrawField_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void Thickness_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                Convert.ToDouble(Thickness.Text);
            }
            catch (Exception)
            {
                Thickness.Text = "";

            }
            
        }

        private void btnDraw_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void rdbSquare_Checked(object sender, RoutedEventArgs e)
        {
            drawer = new DrawSquare();   
        }

        private void rdbLine_Checked(object sender, RoutedEventArgs e)
        {
            drawer = new DrawLine();
        }

        private void rdbDraw_Checked(object sender, RoutedEventArgs e)
        {
            drawer = new DrawPoint();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            cavDrawField.Children.Clear();
            Border b = new Border();
            cavDrawField.Children.Add(b);
            b.BorderBrush = Brushes.Black;
            b.BorderThickness = new Thickness(1);
            b.Height = cavDrawField.Height;
            b.Width = cavDrawField.ActualWidth;
       
            if(rdbDraw.IsChecked.Value)
                drawer = new DrawPoint();
            if(rdbSquare.IsChecked.Value)
                drawer = new DrawSquare();
            if(rdbLine.IsChecked.Value)
                drawer = new DrawLine();
        }
    }
}
