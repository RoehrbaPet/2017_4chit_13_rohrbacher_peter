﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Paint
{
    interface IDrawBehavior
    {
        Shape Draw(Point p, Canvas DrawField, double thickness, Color c);
    }
}
