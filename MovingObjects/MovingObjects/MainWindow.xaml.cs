﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MovingObjects
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Circle circlecircle;
        private Rectangle rectrect;
        private Triangle triangletriangle;
        private int score;
        public MainWindow()
        {
            InitializeComponent();
            circlecircle = new Circle(cavCanvas, circle);
            rectrect = new Rectangle(cavCanvas, rect);
            triangletriangle = new Triangle(cavCanvas, triangle);
            lbxShapes.Items.Add(circlecircle);
            lbxShapes.Items.Add(rectrect);
            lbxShapes.Items.Add(triangletriangle);
            lbxShapes.SelectedItem = lbxShapes.Items[0];

        }

        private void rnbGreen_Click(object sender, RoutedEventArgs e)
        {
            ((AMovingObject)lbxShapes.SelectedItem).shape.Fill = Brushes.Green;
        }

        private void rnbRed_Click(object sender, RoutedEventArgs e)
        {
            ((AMovingObject)lbxShapes.SelectedItem).shape.Fill = Brushes.Red;
        }

        private void rnbBlue_Click(object sender, RoutedEventArgs e)
        {
            ((AMovingObject)lbxShapes.SelectedItem).shape.Fill = Brushes.Blue;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            
                ((AMovingObject) lbxShapes.SelectedItem).physicsTimer.IsEnabled =
                    !((AMovingObject) lbxShapes.SelectedItem).physicsTimer.IsEnabled;
            ((AMovingObject) lbxShapes.SelectedItem).IsMoving = !((AMovingObject) lbxShapes.SelectedItem).IsMoving;
        }

        private void chkFast_Checked(object sender, RoutedEventArgs e)
        {
            
                ((AMovingObject)lbxShapes.SelectedItem).setSpeed(10);
            
        }

        private void circle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (circlecircle.IsMoving)
                score++;
            SetScore();
        }

        private void triangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (triangletriangle.IsMoving)
                score++;
            SetScore();
        }

        private void rect_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (rectrect.IsMoving)
                score++;
            SetScore();
        }

        private void SetScore()
        {
            txtScore.Text = score.ToString();
        }

        private void chkFast_Unchecked(object sender, RoutedEventArgs e)
        {
            ((AMovingObject)lbxShapes.SelectedItem).setSpeed(5);
        }

        private void lbxShapes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            determineStatus(((AMovingObject)lbxShapes.SelectedItem).shape);
            
        }

        private void determineStatus(Shape shape)
        {
            
                
        }
    }
}
