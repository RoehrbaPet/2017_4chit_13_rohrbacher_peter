﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MovingObjects
{
    abstract class AMovingObject
    {
        public bool IsMoving { get; set; }

        protected Canvas cavCanvas;
        private int speed = 5;
        protected int leftspeed = 5;
        protected int upspeed = 5;
        public Shape shape;
        private int interval = 10;
        public DispatcherTimer physicsTimer = new DispatcherTimer();

        public virtual void Move(object sender, EventArgs eventArgs)
        {
            MoveUp();
            MoveLeft();
            physicsTimer.Interval = TimeSpan.FromMilliseconds(interval);
        }

        public virtual void setInterval(int interval)
        {
            this.interval = interval;
        }

        public virtual void setSpeed(int speed)
        {
            if (upspeed < 0)
                upspeed = speed*(-1);
            else
            {
                upspeed = speed;
            }
            if (leftspeed < 0)
                leftspeed = speed*(-1);
            else
            {
                leftspeed = speed;
            }
        }

        protected virtual void MoveUp()
        {

            if (Canvas.GetTop(shape) + shape.Height > cavCanvas.ActualHeight || (Canvas.GetTop(shape)) < 0)
                upspeed = upspeed*(-1);
            Canvas.SetTop(shape, Canvas.GetTop(shape) + upspeed);

        }

        protected virtual void MoveLeft()
        {
            if (Canvas.GetLeft(shape) + shape.Width > cavCanvas.ActualWidth || Canvas.GetLeft(shape) < 0)
                leftspeed = leftspeed*(-1);
            Canvas.SetLeft(shape, Canvas.GetLeft(shape) + leftspeed);
        }
    }

    class Circle : AMovingObject
    {
        public Circle(Canvas cavCanvas, Ellipse circle)
        {
            base.cavCanvas = cavCanvas;
            base.shape = circle;
            physicsTimer.Tick += Move;
        }

        public override string ToString()
        {
            return "Circle";
        }
    }

    class Rectangle : AMovingObject
    {
        public Rectangle(Canvas cavCanvas, System.Windows.Shapes.Rectangle rect)
        {
            base.cavCanvas = cavCanvas;
            base.shape = rect;
            physicsTimer.Tick += Move;
        }

        public override string ToString()
        {
            return "Rectangle";
        }
    }
    class Triangle : AMovingObject
    {
        public Triangle(Canvas cavCanvas, Polygon triangle)
        {
            base.cavCanvas = cavCanvas;
            base.shape = triangle;
            physicsTimer.Tick += Move;
        }
        protected override void MoveUp()
        {

            if (Canvas.GetTop(shape) + shape.ActualHeight > cavCanvas.ActualHeight || (Canvas.GetTop(shape)) < 0)
                base.upspeed = base.upspeed * (-1);
            Canvas.SetTop(shape, Canvas.GetTop(shape) + base.upspeed);

        }

        protected override void MoveLeft()
        {
            if (Canvas.GetLeft(shape) + shape.ActualWidth > cavCanvas.ActualWidth || Canvas.GetLeft(shape) < 0)
                base.leftspeed = base.leftspeed * (-1);
            Canvas.SetLeft(shape, Canvas.GetLeft(shape) + base.leftspeed);
        }

        public override string ToString()
        {
            return "Triangle";
        }
    }
}
